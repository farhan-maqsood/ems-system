-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2015 at 01:54 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `emsdatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `actionitems`
--

CREATE TABLE IF NOT EXISTS `actionitems` (
  `taskname` int(11) NOT NULL,
  `completiontime` datetime NOT NULL,
  `status` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `priorityid` int(11) NOT NULL,
  `assignedto` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `attachment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendies`
--

CREATE TABLE IF NOT EXISTS `attendies` (
  `mid` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conflicts`
--

CREATE TABLE IF NOT EXISTS `conflicts` (
  `conflictid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `resolvedate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emstools`
--

CREATE TABLE IF NOT EXISTS `emstools` (
  `toolid` int(11) NOT NULL,
  `name` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `facilitationems`
--

CREATE TABLE IF NOT EXISTS `facilitationems` (
  `itemid` int(11) NOT NULL,
  `toolid` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `reminder` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `facilitationemsparticipants`
--

CREATE TABLE IF NOT EXISTS `facilitationemsparticipants` (
  `mid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `fbid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `itemid` int(11) NOT NULL,
  `name` int(11) NOT NULL COMMENT 'item name will be here'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meetingagenda`
--

CREATE TABLE IF NOT EXISTS `meetingagenda` (
  `agendaid` int(7) NOT NULL,
  `agendatopic` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL,
  `attachment` varchar(2000) NOT NULL,
  `mid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meetingdetails`
--

CREATE TABLE IF NOT EXISTS `meetingdetails` (
  `mid` int(11) NOT NULL,
  `title` text NOT NULL,
  `mtype` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meetingnum` int(11) NOT NULL,
  `attachment` varchar(1000) NOT NULL,
  `location` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meetingtype`
--

CREATE TABLE IF NOT EXISTS `meetingtype` (
  `typeid` int(11) NOT NULL,
  `mtypeid` int(11) NOT NULL,
  `typename` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storyboard`
--

CREATE TABLE IF NOT EXISTS `storyboard` (
  `sid` int(11) NOT NULL,
  `stype` varchar(100) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  `priorityid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userid` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(300) NOT NULL,
  `usertype` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE IF NOT EXISTS `usertype` (
  `userid` int(11) NOT NULL,
  `typename` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actionitems`
--
ALTER TABLE `actionitems`
  ADD PRIMARY KEY (`taskname`);

--
-- Indexes for table `conflicts`
--
ALTER TABLE `conflicts`
  ADD PRIMARY KEY (`conflictid`);

--
-- Indexes for table `emstools`
--
ALTER TABLE `emstools`
  ADD PRIMARY KEY (`toolid`);

--
-- Indexes for table `facilitationems`
--
ALTER TABLE `facilitationems`
  ADD PRIMARY KEY (`itemid`,`toolid`);

--
-- Indexes for table `facilitationemsparticipants`
--
ALTER TABLE `facilitationemsparticipants`
  ADD PRIMARY KEY (`mid`,`itemid`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`fbid`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`itemid`) COMMENT 'primary key for item id';

--
-- Indexes for table `meetingdetails`
--
ALTER TABLE `meetingdetails`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `meetingtype`
--
ALTER TABLE `meetingtype`
  ADD PRIMARY KEY (`typeid`);

--
-- Indexes for table `storyboard`
--
ALTER TABLE `storyboard`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emstools`
--
ALTER TABLE `emstools`
  MODIFY `toolid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meetingdetails`
--
ALTER TABLE `meetingdetails`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meetingtype`
--
ALTER TABLE `meetingtype`
  MODIFY `typeid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `meetingdetails`
--
ALTER TABLE `meetingdetails`
ADD CONSTRAINT `meetingType` FOREIGN KEY (`mid`) REFERENCES `meetingtype` (`typeid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
